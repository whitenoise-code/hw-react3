﻿import { Alert, Container } from "reactstrap";
import { Link } from "react-router-dom";


const NotFound = () => {

  return(
      <Alert color="danger">
        Страница не найдена. <Link to="/home">На главную</Link>.
      </Alert>
  );
}

export default NotFound;