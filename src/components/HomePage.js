﻿import { Container } from "reactstrap";
import { Link } from "react-router-dom";


const HomePage = ({text}) => {

  return(
      <Container>
        <h3>Главная страница</h3>
        <p>{text}</p>
      </Container>
  );
}

export default HomePage;